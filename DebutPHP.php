<?php
	//Variables
	$v = "";
	$t = 15;
	$i = 0;
	$var = "Variable concaténée";
	$hex = "Elle aussi";
	//Commandes
	echo "Hello world ! <br><br>";
	
	//Conditions
	if($v == "test")
	{
		echo "Hi PHP !";
	}
	else
	{
		echo "Not equal !  <br> ";
	}
	
	//Boucles
	while($i<10)
	{
		$i++;
		echo " <br> Loop... variable i= ".$i;
	}
	
	echo "<br>";
	
	for($j=0;$j<10;$j++)
	{
		echo " <br> Valeur de la variable j: ".$j;
	}
	
	
	//concaténation
	echo "<br><br> Les variables concaténées : ".$var." -> ".$hex;
	
	//Include
	include("Include.php");
	
?>