<?php 

//fonction
function testFunc($param1, $param2)
{
	echo $param1." ".$param2;
}

//classe
class BDD
{
	private $maPrivVar = 0;
	
	public function __construct()
	{
		echo "BDD Obj créé !";
	}
	
	//getters
	public function maPrivVar()
	{
		return $this->maPrivVar;
	}
	
	//setters
	public function setMaPrivVar($new_val)
	{
		if($new_val > 0 AND $new_val < 255)
		{
			$this->maPrivVar = $new_val;
		}
		else
		{
			echo "Erreur: maPrivVar(0-255)";
		}
	}
}

//appel de la classe
$obj = new BDD;
echo $obj->setMaPrivVar(-2)."<br>";
echo $obj->maPrivVar();

?>