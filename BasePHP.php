<?php

//Connexion a la BDD en ligne				bddtp 		sur MariaDB
// 						 HOST        USER  PW  DbNAME
$connexion = new mysqli("localhost","root","","bddtp");

//Test de la connexion
if ($connexion->connect_error) {
	die("Echec de la connexion: ".$connexion->connect_error);
}
else
{
	echo "Connexion a la BDD MariaDB effectuée <br>Récupération des ID";
}

//Requete select BDD mariaDB (tout dans la table users)
$request = $connexion->query('SELECT * FROM users');
//affichage
while ($donnees = $request->fetch_assoc())
{
	echo "<br>".$donnees['id'];
}

//			SQLITE (version fichier local) 			bdd_saine.sqlite
try{
	//Connexion a la BDD fichier
	$sqlite = new PDO('sqlite:bdd_saine.sqlite');

	//Gestion des erreurs
	$sqlite->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
	$sqlite->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	
	echo"<br><br>Connexion a la base SQlite bddtp <br>Récupération des ID <br>";
}	catch (Exception $e) {
	echo "Erreur fatale : connexion è la BDD impossible <br>";
}


//Requete select (bdd fichier)
$query = $sqlite->prepare("SELECT * FROM devices");
$query->execute();

//Lecture du tableau récupéré avec query
while($row = $query->fetch(PDO::FETCH_ASSOC))
{
	echo $row['id']."<br>";
}


?>